package theater;
public class TheaterManagement {
	
	int ticketPrices[][];
	int price = 0;
	int priceZone[][];
	int reserveZone[][];
	DataSeatPrice ticket = new DataSeatPrice();
	public TheaterManagement () {
		ticketPrices = new int[ticket.getTicket().length][ticket.getTicket()[0].length];
		priceZone = new int[ticket.getTicket().length][ticket.getTicket()[0].length];
		reserveZone = new int[ticket.getTicket().length][ticket.getTicket()[0].length];
		for(int i=0;i<ticket.getTicket().length;i++){
		    for(int j=0;j<ticket.getTicket()[0].length;j++){
		    	ticketPrices[i][j] = ticket.getTicket()[i][j];
		    }
		}
	}
	public int getPrice(String row, int seat){
		if (row.equals("A")){
			if((ticketPrices[0][seat-1]== 0)||(ticketPrices[0][seat-1]== -1)){
				return price+=0;
			}
			else{
				price += ticketPrices[0][seat-1];
			    ticketPrices[0][seat-1] = 0;
			}
		}
			
		else if (row.equals("B")){
			if((ticketPrices[1][seat-1]== 0)||(ticketPrices[1][seat-1]== -1)){
				return price+=0;
			}
			else{
				price += ticketPrices[1][seat-1];
			    ticketPrices[1][seat-1] = 0;
			}
		}
		
		else if (row.equals("C")){
			if((ticketPrices[2][seat-1]== 0)||(ticketPrices[2][seat-1]== -1)){
				return price+=0;
			}
			else{
				price += ticketPrices[2][seat-1];
			    ticketPrices[2][seat-1] = 0;
			}
		}
		
		else if (row.equals("D")){
			if((ticketPrices[3][seat-1]== 0)||(ticketPrices[3][seat-1]== -1)){
				return price+=0;
			}
			else{
				price += ticketPrices[3][seat-1];
			    ticketPrices[3][seat-1] = 0;
			}
		}
		
		else if (row.equals("E")){
			if((ticketPrices[4][seat-1]== 0)||(ticketPrices[4][seat-1]== -1)){
				return price+=0;
			}
			else{
				price += ticketPrices[4][seat-1];
			    ticketPrices[4][seat-1] = 0;
			}
		}
		else if (row.equals("F")){
			if((ticketPrices[5][seat-1]== 0)||(ticketPrices[5][seat-1]== -1)){
				return price+=0;
			}
			else{
				price += ticketPrices[5][seat-1];
			    ticketPrices[5][seat-1] = 0;
			}
		}
		else if (row.equals("G")){
			if((ticketPrices[6][seat-1]== 0)||(ticketPrices[6][seat-1]== -1)){
				return price+=0;
			}
			else{
				price += ticketPrices[6][seat-1];
			    ticketPrices[6][seat-1] = 0;
			}
		}
		else if (row.equals("H")){
			if((ticketPrices[7][seat-1]== 0)||(ticketPrices[7][seat-1]== -1)){
				return price+=0;
			}
			else{
				price += ticketPrices[7][seat-1];
			    ticketPrices[7][seat-1] = 0;
			}
		}
		else if (row.equals("I")){
			if((ticketPrices[8][seat-1]== 0)||(ticketPrices[8][seat-1]== -1)){
				return price+=0;
			}
			else{
				price += ticketPrices[8][seat-1];
			    ticketPrices[8][seat-1] = 0;
			}
		}
		else if (row.equals("L")){
			if((ticketPrices[9][seat-1]== 0)||(ticketPrices[9][seat-1]== -1)){
				return price+=0;
			}
			else{
				price += ticketPrices[9][seat-1];
			    ticketPrices[9][seat-1] = 0;
			}
		}
		else if(row.equals("M")){
			if((ticketPrices[10][seat-1]== 0)||(ticketPrices[10][seat-1]== -1)){
				return price+=0;
			}
			else{
				price += ticketPrices[10][seat-1];
			    ticketPrices[10][seat-1] = 0;
			}
		}
		else if (row.equals("N")){
			if((ticketPrices[11][seat-1]== 0)||(ticketPrices[11][seat-1]== -1)){
				return price+=0;
			}
			else{
				price += ticketPrices[11][seat-1];
			    ticketPrices[11][seat-1] = 0;
			}
		}
		else if (row.equals("O")){
			if((ticketPrices[12][seat-1]== 0)||(ticketPrices[12][seat-1]== -1)){
				return price+=0;
			}
			else{
				price += ticketPrices[12][seat-1];
			    ticketPrices[12][seat-1] = 0;
			}
		}
		return price;
	}
	public int[][] seatPrice(int price){
		
		for (int i=0 ;i<15;i++ ){
			for (int j=0 ; j<20;j++){
				if( ticketPrices[i][j]== price){
					priceZone[i][j]= 1;
				}
				else{
					priceZone[i][j]= 0;
				}
			}
		}
		return priceZone;
	}
	public int[][] emptyZone(){
		for (int i=0 ;i<15;i++ ){
			for (int j=0 ; j<20;j++){
				if( ticketPrices[i][j]== 0){
					 reserveZone[i][j] = 1;
				}
				else{
					reserveZone[i][j] = 0;
				}
			}
		}
	    return    reserveZone;
	}
	public int[][] getSeatPrice(){
		return ticketPrices;
	}
	public int[][] getSeat(){
		return priceZone;
	}
}